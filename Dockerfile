# -----
# Build
# -----
FROM maven:3-jdk-11 AS build-server
WORKDIR /build

# dependencies
COPY pom.xml .
RUN mvn dependency:go-offline

# source
COPY src/. /build/src/
RUN mvn package -DskipTests

# -----
# Release
# -----
FROM adoptopenjdk/openjdk11:alpine-jre
RUN apk add --no-cache bash

COPY --from=build-server /build/target/quality-cli-*.jar quality-cli.jar

ENTRYPOINT ["java", "-jar", "quality-cli.jar"]