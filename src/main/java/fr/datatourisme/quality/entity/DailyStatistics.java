/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.quality.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.time.LocalDate;
import java.util.List;

@Document(indexName = "statistics-#{@startDate.format(T(java.time.format.DateTimeFormatter).ofPattern(\"yyyy.MM\"))}")
@Data
@Accessors(chain = true)
public class DailyStatistics {
    @Id
    private String id;

    @Field(type = FieldType.Date)
    private LocalDate date;

    @Field(type = FieldType.Keyword)
    private String hasBeenCreatedBy;

    @Field(type = FieldType.Keyword)
    private String hasOrganizationIdentifier;

    @Field(type = FieldType.Long)
    private long objectCount;

    @Field(type = FieldType.Long)
    private long abnormalObjectCount;

    @Field(type = FieldType.Long)
    private long anomalyCount;

    @Field(type = FieldType.Nested, includeInParent = true)
    private List<AnomalyStatistics> anomalies;

    @Data
    @Accessors(chain = true)
    static public class AnomalyStatistics {
        @Field(type = FieldType.Keyword)
        private String analyzer;

        @Field(type = FieldType.Long)
        private long count;

        @Field(type = FieldType.Long)
        private long objectCount;
    }
}
