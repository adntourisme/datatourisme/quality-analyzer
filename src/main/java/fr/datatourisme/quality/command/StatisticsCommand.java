/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.quality.command;

import fr.datatourisme.quality.entity.DailyStatistics;
import fr.datatourisme.quality.entity.PointOfInterest;
import fr.datatourisme.quality.repository.DailyStatisticRepository;
import lombok.SneakyThrows;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.filter.FilterAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.filter.ParsedFilter;
import org.elasticsearch.search.aggregations.bucket.nested.NestedAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.nested.ParsedNested;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import picocli.CommandLine.Command;
import picocli.CommandLine.Model;
import picocli.CommandLine.Spec;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Command(
    name = "statistics",
    description = "Compute POI statistics for the current day",
    mixinStandardHelpOptions = true
)
public class StatisticsCommand implements Runnable {
    @Spec
    Model.CommandSpec spec;

    @Autowired
    RestHighLevelClient client;

    @Autowired
    ElasticsearchOperations elasticsearchOperations;

    @Autowired
    DailyStatisticRepository dailyStatisticRepository;

    @Autowired
    LocalDate startDate;

    @Override
    @SneakyThrows
    public void run() {
        // first : purge current date from stats
        dailyStatisticRepository.deleteByDate(startDate);

        IndexCoordinates indexCoordinates = elasticsearchOperations.getIndexCoordinatesFor(PointOfInterest.class);

        // aggregation to count abnormal objects (global and per anomaly type)
        FilterAggregationBuilder abnormalAggregationBuilder = AggregationBuilders
            .filter("abnormal", QueryBuilders.rangeQuery("analyze.anomalyStatistics.total").gt(0))
            .subAggregation(AggregationBuilders.terms("anomalies")
                .field("analyze.anomalies.analyzer"));

        // aggregation to count anomalies (global and per anomaly type)
        NestedAggregationBuilder anomaliesAggregationBuilder = AggregationBuilders
            .nested("anomalies", "analyze.anomalies")
            .subAggregation(AggregationBuilders.terms("analyzer")
                .field("analyze.anomalies.analyzer"));

        TermsAggregationBuilder organizationAggregationBuilder = AggregationBuilders.terms("hasOrganizationIdentifier")
            .field("hasOrganizationIdentifier")
            .size(10000)
            .subAggregation(abnormalAggregationBuilder)
            .subAggregation(anomaliesAggregationBuilder)
            .subAggregation(
                AggregationBuilders.terms("hasBeenCreatedBy")
                    .field("hasBeenCreatedBy.uri")
                    .size(10000)
                    .subAggregation(abnormalAggregationBuilder)
                    .subAggregation(anomaliesAggregationBuilder)
            );

        SearchSourceBuilder builder = new SearchSourceBuilder()
            .size(0)
            .trackTotalHits(true)
            .aggregation(abnormalAggregationBuilder)
            .aggregation(anomaliesAggregationBuilder)
            .aggregation(organizationAggregationBuilder);

        // go search
        SearchRequest searchRequest = new SearchRequest().indices(indexCoordinates.getIndexNames()).source(builder);
        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);

        // prepare statistics list
        List<DailyStatistics> dailyStatisticsList = new ArrayList<>();

        // main stat
        long totalHits = response.getHits().getTotalHits().value;
        DailyStatistics statistics = process(totalHits, response.getAggregations());
        dailyStatisticsList.add(statistics);

        // foreach organization
        ParsedStringTerms organizationAggregation = response.getAggregations().get("hasOrganizationIdentifier");
        organizationAggregation.getBuckets().forEach(bucket -> {
            DailyStatistics organizationStatistics = process(bucket.getDocCount(), bucket.getAggregations())
                .setHasOrganizationIdentifier(bucket.getKeyAsString());
            dailyStatisticsList.add(organizationStatistics);

            // foreach creator
            ParsedStringTerms creatorAggregation = bucket.getAggregations().get("hasBeenCreatedBy");
            creatorAggregation.getBuckets().forEach(bucket2 -> {
                DailyStatistics creatorStatistics = process(bucket2.getDocCount(), bucket2.getAggregations())
                    .setHasBeenCreatedBy(bucket2.getKeyAsString())
                    .setHasOrganizationIdentifier(bucket.getKeyAsString());
                dailyStatisticsList.add(creatorStatistics);
            });
        });

        // save in es
        dailyStatisticRepository.saveAll(dailyStatisticsList);
    }

    /**
     * Process aggregations to prepare DailyStatistics
     *
     * @param objectCount
     * @param aggregations
     * @return
     */
    private DailyStatistics process(long objectCount, Aggregations aggregations) {
        ParsedFilter abnormalAggregation = aggregations.get("abnormal");
        ParsedNested anomaliesAggregation = aggregations.get("anomalies");

        Map<String, DailyStatistics.AnomalyStatistics> anomalyStatisticsMap = new HashMap<>();
        ((ParsedStringTerms) abnormalAggregation.getAggregations().get("anomalies")).getBuckets().forEach(bucket -> {
            anomalyStatisticsMap.computeIfAbsent(bucket.getKeyAsString(), analyzer -> new DailyStatistics.AnomalyStatistics().setAnalyzer(analyzer))
                .setObjectCount(bucket.getDocCount());
        });

        ((ParsedStringTerms) anomaliesAggregation.getAggregations().get("analyzer")).getBuckets().forEach(bucket -> {
            anomalyStatisticsMap.computeIfAbsent(bucket.getKeyAsString(), analyzer -> new DailyStatistics.AnomalyStatistics().setAnalyzer(analyzer))
                .setCount(bucket.getDocCount());
        });

        List<DailyStatistics.AnomalyStatistics> anomalies = new ArrayList<>(anomalyStatisticsMap.values());

        return new DailyStatistics()
            .setDate(startDate)
            .setObjectCount(objectCount)
            .setAbnormalObjectCount(abnormalAggregation.getDocCount())
            .setAnomalyCount(anomaliesAggregation.getDocCount())
            .setAnomalies(anomalies);
    }
}

/**
 * {
 *   "size": 0,
 *   "track_total_hits": 2147483647,
 *   "aggs": {
 *     "abnormal": {
 *       "filter": {
 *         "range": {
 *           "analyze.anomalyStatistics.total": {
 *             "gt": 0
 *           }
 *         }
 *       },
 *       "aggs": {
 *         "anomalies": {
 *           "terms": {
 *             "field": "analyze.anomalies.analyzer"
 *           }
 *         }
 *       }
 *     },
 *     "anomalies": {
 *       "nested": {
 *         "path": "analyze.anomalies"
 *       },
 *       "aggs": {
 *         "analyzer": {
 *           "terms": {
 *             "field": "analyze.anomalies.analyzer"
 *           }
 *         }
 *       }
 *     }
 *   }
 * }
 */