/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.quality.command;

import picocli.CommandLine.Command;
import picocli.CommandLine.Model;
import picocli.CommandLine.Spec;

@Command(
    name = "purge",
    description = "Launch a daily analyze of points of interest",
    mixinStandardHelpOptions = true
)
public class PurgeCommand implements Runnable {

    @Spec
    Model.CommandSpec spec;

    @Override
    public void run() {
        spec.commandLine().getOut().println("Hello !");
    }
}
