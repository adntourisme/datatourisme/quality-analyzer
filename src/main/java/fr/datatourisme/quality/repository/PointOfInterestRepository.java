/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.quality.repository;

import fr.datatourisme.quality.entity.PointOfInterest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.convert.ElasticsearchConverter;
import org.springframework.data.elasticsearch.core.document.Document;
import org.springframework.data.elasticsearch.core.query.UpdateQuery;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class PointOfInterestRepository {

    @Autowired
    protected ElasticsearchOperations elasticsearchOperations;

    @Autowired
    protected ElasticsearchConverter elasticsearchConverter;

    public void update(Collection<PointOfInterest> entities) {
        List<UpdateQuery> queries = entities.stream().map(entity -> {
            Document document = elasticsearchConverter.mapObject(entity);
            document.remove("_class");

            return UpdateQuery.builder(entity.getUri()).withDocument(document).build();
        }).collect(Collectors.toList());

        elasticsearchOperations.bulkUpdate(queries, PointOfInterest.class);
    }
}
