/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.quality.analyzer;
import fr.datatourisme.quality.entity.Anomaly;

import java.util.List;
import java.util.Set;

public interface AnalyzerInterface {
    List<Anomaly> analyze(Set<AnalyzeItem> items);
}
