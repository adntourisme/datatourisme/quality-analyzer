/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.quality.analyzer;

import fr.datatourisme.quality.entity.Anomaly;
import org.apache.commons.jxpath.Pointer;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Incohérence dans les moyens de communication
 * Incohérence dans les dates
 * Incohérence dans les catégories
 */
@Component
public class ConsistencyAnalyzer extends AbstractAsyncAnalyzer {
    @Override
    public List<Anomaly> analyze(AnalyzeItem item) {
        List<Anomaly> anomalies = new ArrayList<>();

        // incohérence dans les dates
        Map<String, Pointer> map = item.getPointers("//startDate");
        map.forEach((path, pointer) -> {
            String startDate = (String) pointer.getValue();
            String endDate = item.getSingleValue(pointer, "../endDate");
            if (endDate != null && endDate.compareTo(startDate) < 0) {
                String parentPath = AnalyzeItem.asPath(item.getContext().getRelativeContext(pointer).getPointer(".."));

                Anomaly anomaly = new Anomaly()
                    .setPath(parentPath)
                    .setValue(endDate)
                    .setMessage("Dates incohérentes")
                    .setDescription(String.format("La date de fin %s est antérieure à la date de début %s", endDate, startDate));

                anomalies.add(anomaly);
            }
        });

        // @todo Incohérence dans les catégories

        // @todo Incohérence dans les moyens de communication

        return anomalies;
    }
}
