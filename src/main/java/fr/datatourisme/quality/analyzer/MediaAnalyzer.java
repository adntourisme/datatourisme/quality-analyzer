/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.quality.analyzer;

import fr.datatourisme.quality.entity.Anomaly;
import org.apache.commons.jxpath.Pointer;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class MediaAnalyzer extends AbstractAsyncAnalyzer {

    @Override
    public List<Anomaly> analyze(AnalyzeItem item) {
        List<Anomaly> anomalies = new ArrayList<>();

        Map<String, Pointer> map = item.getPointers("hasRepresentation");
        if (map.isEmpty()) {
            Anomaly anomaly = new Anomaly()
                .setPath("hasRepresentation")
                .setMessage("Aucun média")
                .setDescription("Le point d'interêt n'a pas média.");

            anomalies.add(anomaly);
        }

        map.forEach((path, pointer) -> {
            String credits = item.getSingleValue(pointer, "hasAnnotation/credits");
            if (credits == null) {
                Anomaly anomaly = new Anomaly()
                    .setPath(path)
                    .setMessage("Aucun crédit")
                    .setDescription("Le média n'a pas de crédit associé.");

                anomalies.add(anomaly);
            }
        });

        return anomalies;
    }
}
