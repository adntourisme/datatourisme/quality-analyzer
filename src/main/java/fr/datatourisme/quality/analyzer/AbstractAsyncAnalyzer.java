/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.quality.analyzer;

import fr.datatourisme.quality.entity.Anomaly;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.AsyncTaskExecutor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

abstract public class AbstractAsyncAnalyzer implements AnalyzerInterface {

    @Autowired
    AsyncTaskExecutor taskExecutor;

    @Override
    public List<Anomaly> analyze(Set<AnalyzeItem> items) {
        List<CompletableFuture<List<Anomaly>>> futures = new ArrayList<>();

        for (AnalyzeItem item : items) {
            CompletableFuture<List<Anomaly>> future = CompletableFuture.supplyAsync(() -> analyze(item), taskExecutor)
                .exceptionally(throwable ->  {
                    Anomaly anomaly = new Anomaly()
                        .setMessage("Erreur de traitement")
                        .setDescription(throwable.getMessage());

                    return List.of(anomaly);
                })
                .thenApply(anomalies -> {
                    return anomalies.stream()
                        .filter(Objects::nonNull)
                        .peek(anomaly -> {
                            // finalize anomalies
                            anomaly.setUri(item.getUri());
                        })
                        .collect(Collectors.toList());
                    }
                );
            futures.add(future);
        }

        return futures.stream().flatMap(future -> future.join().stream()).collect(Collectors.toList());
    }

    /**
     * @param item
     * @return
     */
    abstract public List<Anomaly> analyze(AnalyzeItem item);
}
