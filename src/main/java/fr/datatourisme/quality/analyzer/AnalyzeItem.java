/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.quality.analyzer;

import lombok.Builder;
import lombok.Data;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.JXPathInvalidAccessException;
import org.apache.commons.jxpath.JXPathNotFoundException;
import org.apache.commons.jxpath.Pointer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Data
@Builder
public class AnalyzeItem {
    private final String uri;
//    private final String hasBeenCreatedBy;
//    private final Integer hasOrganizationIdentifier;
    private final JXPathContext context;

    /**
     * Create from URI and context obj
     */
    public static AnalyzeItem create(String uri, Object contextBean) {
        JXPathContext context = JXPathContext.newContext(contextBean);
//        Number hasOrganizationIdentifier = (Number) context.selectSingleNode("hasOrganizationIdentifier");
//        String hasBeenCreatedBy = (String) context.selectSingleNode("hasBeenCreatedBy/uri");

        return builder()
            .uri(uri)
            .context(context)
//            .hasOrganizationIdentifier(hasOrganizationIdentifier.intValue())
//            .hasBeenCreatedBy(hasBeenCreatedBy)
            .build();
    }

    /**
     * Find mapped by path
     */
    public <T> Map<String, T> getValues(String... paths) {
        return getValues(getContext(), paths);
    }

    @SuppressWarnings("unchecked")
    public <T> Map<String, T> getValues(Pointer pointer, String... paths) {
        return getValues(getContext().getRelativeContext(pointer), paths);
    }

    @SuppressWarnings("unchecked")
    private <T> Map<String, T> getValues(JXPathContext ctx, String... paths) {
        Map<String, T> results = new HashMap<>();
        for (String path : paths) {
            Iterator<Pointer> iterator = ctx.iteratePointers(path);
            iterator.forEachRemaining(p -> results.put(asPath(p), (T) p.getValue()));
        }

        return results;
    }

    /**
     * Find mapped by path
     */
    public Map<String, Pointer> getPointers(String... paths) {
        return getPointers(getContext(), paths);
    }

    public Map<String, Pointer> getPointers(Pointer pointer, String... paths) {
        return getPointers(getContext().getRelativeContext(pointer));
    }

    @SuppressWarnings("unchecked")
    private Map<String, Pointer> getPointers(JXPathContext ctx, String... paths) {
        Map<String, Pointer> results = new HashMap<>();
        for (String path : paths) {
            Iterator<Pointer> iterator = ctx.iteratePointers(path);
            iterator.forEachRemaining(p -> results.put(asPath(p), p));
        }

        return results;
    }

    public <T> T getSingleValue(String path) {
        return getSingleValue(getContext().getContextPointer(), path);
    }

    public <T> T getSingleValue(Pointer pointer, String path) {
        return getSingleValue(getContext().getRelativeContext(pointer), path);
    }

    @SuppressWarnings("unchecked")
    private <T> T getSingleValue(JXPathContext ctx, String path) {
        try {
            Iterator<T> iterator = ctx.iterate(path);
            if (iterator.hasNext()) {
                return iterator.next();
            }

            return null;
        } catch (JXPathNotFoundException | JXPathInvalidAccessException ex) {
            return null;
        }
    }

    public Pointer getSinglePointer(String path) {
        return getSinglePointer(getContext(), path);
    }

    public Pointer getSinglePointer(Pointer pointer, String path) {
        return getSinglePointer(getContext().getRelativeContext(pointer), path);
    }

    @SuppressWarnings("unchecked")
    private Pointer getSinglePointer(JXPathContext ctx, String path) {
        try {
            Iterator<Pointer> iterator = ctx.iteratePointers(path);
            if (iterator.hasNext()) {
                return iterator.next();
            }

            return null;
        } catch (JXPathNotFoundException | JXPathInvalidAccessException ex) {
            return null;
        }
    }

    /**
     * Helper method to rewrite path
     * @param pointer
     * @return
     */
    public static String asPath(Pointer pointer) {
        return pointer.asPath()
            .replace("'][@name='", ".")
            .replace("][@name='", ".")
            .replace("'][", ".")
            .replace("][", ".")
            .replace("/.[@name='", "")
            .replace("']", "")
            .replace("]", "");
    }
}
