/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.quality.analyzer;

import com.github.pemistahl.lingua.api.Language;
import com.github.pemistahl.lingua.api.LanguageDetector;
import com.github.pemistahl.lingua.api.LanguageDetectorBuilder;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *  https://pythonrepo.com/repo/pemistahl-lingua-python-computer-vision
 *  https://github.com/pemistahl/lingua
 */
class LocalizedTextAnalyzerTest {
    @Test
    void analyze() {
        LanguageDetector detector = LanguageDetectorBuilder.fromLanguages(Language.ENGLISH, Language.FRENCH).build();

        System.out.println(detector.computeLanguageConfidenceValues("Meublé de tourisme classé 3 étoiles, type T4, tout équipé en rez-de-chaussée avec terrasse, petit jardinet cloturé, salon de jardin et place de parking privée. Situé entre le 45ème parallèle et le Château du Bouilh (chemin de St Jacques de Compostelle), tout prés du centre ville et des commerces."));

        // see https://github.com/pemistahl/lingua/issues/115
        Language detectedLanguage = detector.detectLanguageOf("Découverte du château grâce à l'application visite virtuelle");
        assertEquals(Language.FRENCH, detectedLanguage);

    }
}