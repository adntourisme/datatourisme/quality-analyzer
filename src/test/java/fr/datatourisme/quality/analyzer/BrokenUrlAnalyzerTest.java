/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.quality.analyzer;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BrokenUrlAnalyzerTest {

    @Test
    void analyze() {
        InputStream resource = getClass().getClassLoader().getResourceAsStream("fixtures/urls.txt");
        List<String> urls =  new BufferedReader(new InputStreamReader(resource,
                StandardCharsets.UTF_8)).lines().collect(Collectors.toList());

    }

//    @Test
//    void testHttp5() throws ExecutionException, InterruptedException {
//        String url = "https://www.collectifpaysan.fr/";
//
//        final PoolingAsyncClientConnectionManager connectionManager = PoolingAsyncClientConnectionManagerBuilder.create()
//            .setTlsStrategy(ClientTlsStrategyBuilder.create()
//                .setSslContext(SSLContexts.createSystemDefault())
//                .setTlsVersions(TLS.V_1_3, TLS.V_1_2)
//                .build())
//            .setPoolConcurrencyPolicy(PoolConcurrencyPolicy.LAX) // important, to preserve result quality
//            .setConnPoolPolicy(PoolReusePolicy.LIFO)
//            .setConnectionTimeToLive(TimeValue.ofMinutes(1L))
//            .build();
//
//        CloseableHttpAsyncClient client = HttpAsyncClients.custom()
//            .setConnectionManager(connectionManager)
//            .setDefaultRequestConfig(RequestConfig.custom()
//                .setConnectTimeout(Timeout.ofSeconds(5))
//                .setResponseTimeout(Timeout.ofSeconds(5))
//                .setCookieSpec(StandardCookieSpec.STRICT)
//                .build())
//            .setVersionPolicy(HttpVersionPolicy.NEGOTIATE)
//            .build();
//
//        client.start();
//
//        SimpleHttpRequest request = SimpleRequestBuilder.get()
//            .setUri(url)
//            .setHeader("user-agent", BrokenUrlAnalyzer.USER_AGENT)
//            .build();
//
//        CompletableFuture<Integer> completableFuture = new CompletableFuture<Integer>()
//            .orTimeout(5, TimeUnit.SECONDS);
//
//        client.execute(
//            SimpleRequestProducer.create(request),
//            SimpleResponseConsumer.create(),
//            new FutureCallback<SimpleHttpResponse>() {
//
//                @Override
//                public void completed(final SimpleHttpResponse response) {
//                    completableFuture.complete(response.getCode());
//                }
//
//                @Override
//                public void failed(final Exception ex) {
//                    completableFuture.completeExceptionally(ex);
//                }
//
//                @Override
//                public void cancelled() {
//                    completableFuture.cancel(true);
//                }
//
//            });
//
//        Integer integer = completableFuture.get();
//        System.out.println(integer);
//
//        client.close(CloseMode.GRACEFUL);
//    }

    /**
     * see https://bugs.openjdk.java.net/browse/JDK-8238579
     * see https://stackoverflow.com/questions/54485755/java-11-httpclient-leads-to-endless-ssl-loop
     *
     * The bug was fixed for the 11.0.10 Java version
     *
     * @throws InterruptedException
     * @throws URISyntaxException
     * @throws IOException
     */
    @Test
    void testJDK11Bug() throws InterruptedException, URISyntaxException, IOException {

        HttpClient httpClient =  HttpClient.newBuilder()
            .followRedirects(HttpClient.Redirect.ALWAYS)
            .connectTimeout(Duration.of(5, ChronoUnit.SECONDS))
            .build();

        HttpRequest request = HttpRequest.newBuilder().GET()
            .header("user-agent", LinksAnalyzer.USER_AGENT)
            .timeout(Duration.of(5, ChronoUnit.SECONDS))
            .uri(new URI("https://www.lechampdeslys.com"))
            .build();
//
//        Integer integer = httpClient.sendAsync(request, HttpResponse.BodyHandlers.discarding())
//            .thenApply(HttpResponse::statusCode).get();

        Integer integer = httpClient.send(request, HttpResponse.BodyHandlers.discarding()).statusCode();
        assertEquals(200, integer);
        System.out.println(integer);
    }

}